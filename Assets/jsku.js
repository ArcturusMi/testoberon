var checkbox = document.querySelector('input[name=theme]');

checkbox.addEventListener('click', function() {
    if (this.checked) {
        trans()
        document.getElementById('body').setAttribute('id', 'bodyd')
    } else {
        trans()
        document.getElementById('bodyd').setAttribute('id', 'body')
    }
})

let trans = () => {
    document.documentElement.classList.add('transition');
    window.setTimeout(() => {
        document.documentElement.classList.remove('transition')
    }, 1000)
}