$(document).ready(() => {
    $("#submit").click(() => {
        let key = $("#search").val();

        $.ajax({
            method: 'GET',
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function (response) {
                //console.log(response);
                $("#content").empty();
                var contents = response.items
                for (var i = 0; i < contents.length; i++) {
                    var row = document.createElement("tr");

                    var numRow = document.createElement("th")
                    numRow.innerHTML = i + 1;
                    $(row).append(numRow);



                    var thumbnail = document.createElement("td");
                    var img = document.createElement("img");
                    try {
                        img.src = contents[i].volumeInfo.imageLinks.thumbnail;
                        img.alt = "NotFound"
                    } catch (error) {
                        img.alt = "NotFound"
                    }

                    $(thumbnail).append(img);
                    $(row).append(thumbnail);

                    var name = document.createElement("td");
                    name.innerHTML = contents[i].volumeInfo.title;
                    $(row).append(name);


                    var description = document.createElement("td");
                    var authors = "";
                    if (typeof (contents[i].volumeInfo.authors) == 'undefined') {
                        $(description).append("No author information");
                    }
                    else {
                        for (var j = 0; j < contents[i].volumeInfo.authors.length; j++) {
                            var authors = authors + contents[i].volumeInfo.authors[j];
                            if (j != contents[i].volumeInfo.authors.length - 1) {
                                var authors = authors + ", ";
                            }
                        }
                        console.log(authors);
                        $(description).append(authors);
                    }
                    $(row).append(description);

                    $("#content").append(row);
                }

            }
        })
    })

})