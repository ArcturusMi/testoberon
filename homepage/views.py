from django.shortcuts import render,redirect
from .models import Status    ##CHANGE
from . import forms

# Create your views here.

def home(request):
    title = "HOME"
    status = Status.objects.all()

    if request.method == 'POST':
        form = forms.AddStatus(request.POST)
        if form.is_valid():
            form.save()
            return redirect("homepage:homepage")

    else:
        form = forms.AddStatus()
    return render(request, 'homepage.html', {"title" :title,"form" : form,"status":status})