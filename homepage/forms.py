from django import forms
from . import models

class AddStatus(forms.ModelForm):
    class Meta:
        model = models.Status
        fields = ['title']