from django.test import TestCase,Client,LiveServerTestCase
from .models import Status
from .views import *
from .urls import *
from datetime import datetime,date, timedelta
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time,os
from Testoberon.settings import BASE_DIR
# Create your tests here.


class StatusUnitTest(TestCase):
    def test_status_exists(self):
        Status.objects.create(title="A",time=datetime.now())
        howmany = Status.objects.all().count()
        self.assertEqual(howmany,1)
        
    
    def test_time_is_correct(self):
        Status.objects.create(title="A")
        time = Status.objects.filter(title="A").get().time
        self.assertEqual(time.strftime("%d/%m/%Y, %H:%M"),datetime.now().strftime("%d/%m/%Y, %H:%M"))


class TemplateCheckUnitTest(TestCase):
    def test_use_template(self):
        satu = Client().get('')
        self.assertTemplateUsed(satu,'homepage.html')

    def test_statuscode(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_postm(self):
        response = Client().post('',{'title':'tes'})
        response = Client().get('')
        self.assertIn('tes',response.content.decode('utf8'))

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-infobars")
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--dns-prefetch-disable")
        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        
        #super(FunctionalTest,self).setUp()
        

    def tearDown(self):
        return self.browser.quit()
    
    def test_myapp(self):
        self.browser.get('http://localhost:8000/')
        time.sleep(3)
        form_box = self.browser.find_element_by_id('id_title')
        form_box.send_keys("SELENIUM IS HERE")
        form_box.submit()
        time.sleep(1)
        self.assertIn("SELENIUM",self.browser.page_source)
        self.tearDown()