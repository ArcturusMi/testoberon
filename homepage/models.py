from django.db import models
from datetime import datetime,date, timedelta
import pytz

# Create your models here.
class Status(models.Model):
    title = models.CharField(max_length=300)
    time = models.DateTimeField(default = datetime.now())