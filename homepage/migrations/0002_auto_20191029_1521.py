# Generated by Django 2.2.6 on 2019-10-29 08:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='time',
            field=models.DateTimeField(),
        ),
    ]
