from django.shortcuts import render,redirect,HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login,logout

# Create your views here.

def index(request):
    if request.user.is_authenticated:
        greetings = "Hello! " + request.user.username
        signupclass = "display:none"
        urlto = "/accounts/logout/"
    else:
        greetings = "Please Login"
        signupclass = ""
        urlto = "/accounts/signup/"
    return render(request, 'accountsblank.html',{'greetings':greetings,"signupclass":signupclass,"urlto":urlto})


def signup_view(request):
    title = "SIGNUP"
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/accounts/')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html',{'form':form})


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/accounts/')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html',{'form':form})


def logout_view(request):
    logout(request)
    return redirect('/accounts/')