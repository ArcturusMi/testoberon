from django.test import TestCase,Client,LiveServerTestCase
from .views import *
from .urls import *
from datetime import datetime,date, timedelta
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time,os
from Testoberon.settings import BASE_DIR

class TemplateCheckUnitTest(TestCase):
    def test_use_template(self):
        satu = Client().get('/accounts/')
        self.assertTemplateUsed(satu,'accountsblank.html')

    def test_statuscode(self):
        response = Client().get('/accounts/')
        self.assertEqual(response.status_code,200)

    
    def test_use_template_signup(self):
        satu = Client().get('/accounts/signup/')
        self.assertTemplateUsed(satu,'signup.html')

    def test_statuscode_signup(self):
        response = Client().get('/accounts/signup/')
        self.assertEqual(response.status_code,200)

    def test_use_template_login(self):
        satu = Client().get('/accounts/login/')
        self.assertTemplateUsed(satu,'login.html')

    def test_statuscode_login(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code,200)