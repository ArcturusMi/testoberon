from django.urls import path
from . import views
app_name = "jsku"
urlpatterns = [
    path('', views.jskuhome, name="home-jsku"),
]
