from django.test import TestCase,Client,LiveServerTestCase
from .views import *
from .urls import *
from datetime import datetime,date, timedelta
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time,os
from Testoberon.settings import BASE_DIR
# Create your tests here.


class TemplateCheckUnitTest(TestCase):
    def test_use_template(self):
        satu = Client().get('/jsku/')
        self.assertTemplateUsed(satu,'jsku.html')

    def test_statuscode(self):
        response = Client().get('/jsku/')
        self.assertEqual(response.status_code,200)

