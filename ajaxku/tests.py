from django.test import TestCase,Client,LiveServerTestCase
from .views import *
from .urls import *
from datetime import datetime,date, timedelta
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time,os
from Testoberon.settings import BASE_DIR

# Create your tests here.
class Story8Test(TestCase):
    def test_url_page(self):
        response = self.client.get("/ajaxku/")
        self.assertEqual(response.status_code,200)

    def test_using_template(self):
        response = self.client.get("/ajaxku/")
        self.assertTemplateUsed(response,"ajaxku.html")
