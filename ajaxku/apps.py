from django.apps import AppConfig


class AjaxkuConfig(AppConfig):
    name = 'ajaxku'
